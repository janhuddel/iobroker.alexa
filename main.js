'use strict';

// get adapter instance
const utils = require(__dirname + '/lib/utils');
const adapter = utils.adapter('alexa');

const Alexa = require('./lib/alexa');
let alexa = undefined;

adapter.on('ready', () => {
    alexa = new Alexa(adapter);
    alexa.startServer();
});

adapter.on('unload', (callback) => {
    try {
        if (alexa) {
            alexa.stopServer();
        }
        callback();
    } catch (e) {
        adapter.log.error(e.message);
        callback();
    }
});

adapter.on('objectChange', (id, obj) => {
    adapter.log.debug('objectChange ' + id + ' ' + JSON.stringify(obj));
});

adapter.on('stateChange', (id, state) => {
    adapter.log.debug('stateChange ' + id + ' ' + JSON.stringify(state));
});

adapter.on('message', (obj) => {
    if (typeof obj == 'object' && obj.message) {
        if (obj.command == 'send') {
            // e.g. send email or pushover or whatever
            console.log('send command');

            // Send response in callback if required
            if (obj.callback) adapter.sendTo(obj.from, obj.command, 'Message received', obj.callback);
        }
    }
});
